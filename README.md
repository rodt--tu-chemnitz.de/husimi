# Husimi function calculation

This programm is for calculating the husimi function from the boundary function (and its radial derivative) of open microcavities.

## Getting started

This module consists of the following parts and their respective purposes:

- `calc`: Husimi function calculation
- `file`: Save and read Husimi functions, read COMSOL files (as the software calculates the boundary functions in the demo)
- `plot`: Plotting Husimi functions.
- `farfield`: Calculate the farfield using the outer emerging Husimi function

For an example of usage also see the [demo](#demo).

## Theory

This programm evaluates the expression

```math
H_j^{\mathrm{inc (em)}}(s, p) = \frac{k_j}{2 \pi} \left|
    (-1)^j \mathcal{F}_j(p) h_j(s,p)
    + (-)
    \frac{\mathrm{i}}{k_0 \mathcal{F}_j(p)} h_j'(s,p)
\right|^2 \quad
```

with the angular momentum weight

```math
\mathcal{F}_j(p) = \sqrt{ n_j \sqrt{1-p^2} }
```

and two contributions

```math
\begin{aligned}
h_j(s,p) &= \int_0^{L} \Psi_j(s') \xi(s'; s, p) \mathrm{d}s' \quad \text{and} \\
h'_j(s,p) &= \int_0^{L} \Psi'_j(s') \xi(s'; s, p) \mathrm{d}s'  \quad \text{.}
\end{aligned}
```

The indizes $j=0$ denotes _outside_, $j=1$ _inside_ the cavity. $L$ is the arclength of the cavity. $\xi(s'; s, p)$ describes a coherent state centered around $(s,p)$ with

```math
\xi(s'; s, p) = \left(\frac{b}{\sigma \pi}\right)^{1/4} \sum_m
\exp\left[ - \frac{b}{2 \sigma}  (s' - s + m L)^2 \right]
\exp\left[ - \mathrm{i} k_j p (s + m L) \right] \quad ,
```

with spatial uncertainty $\sigma = 2 / k_1$ (by default in this programm). $b$ controls the shape of the coherent state and thus contains information about the length scale of the system. Here it is chosen as $b = 1 / R_0$, where $R_0$ is the mean radius of the microcavity.

## Demo

In the following the steps used to calculate and analyze the Husimi function using module are shown

### 1. Obtain boundary functions

The first step to obtain the Husimi function is to calculate the mode pattern. Here, this is done using COMSOL. The resulting mode might look something like this:

![Whispering-gallery mode](readme_images/field_normal.png){width=600}

The black line marks the boundary of the cavity, where the boundary functions are taken.

### 2. Calculate the Husimi functions

The function to calculate the Husimi functions can be found under `husimi.calc.calc_husimi`. It can then be plotted using `husimi.plot.plot_husimi`. In this case, it gives the following images

![inner incoming Husimi](readme_images/husimi_inner_inc.png){width=400}
![outer emerging Husimi](readme_images/husimi_outer_em.png){width=400}

When plotting the root of the Husimi functions, the contributions in the leaky region are more visible.

![inner incoming Husimi](readme_images/husimi_inner_inc_root.png){width=400}
![outer emerging Husimi](readme_images/husimi_outer_em_root.png){width=400}

### 3. Connection between wave farfield and outer, emerging Husimi function

The outer emerging Husimi function shows how much intensity is leaving the cavity at the respective angles at a certain position on the cavity boundary. When all intensities of the outer Husimis are summed up, the farfield obtained by frequency domain calculations can be approximated, even though phase information is lost.

For the farfield intensity at angle $\theta$ all contributions of the Husimi function are added up using

```math
\mathrm{FF}^\mathrm{H}(\theta) = \left| \int_0^L \int_{-1}^{+1} \sqrt{H_0^\mathrm{em}(\theta; s,p)} \, \mathrm{d}p \, \mathrm{d}s \right|^2 \quad .
```

Note that most $(s,p)$ dont contribute to the farfield at $\theta$ due to geometrical reasons. The root is used to keep the calculation analogous to the electric field, where the farfield intensity can be written as $\mathrm{FF}(\theta) = \left| \int_0^L E_z(\theta; s) \mathrm{d}s \right|^2$ for the contributions of the field at the boundary.

The following grafic illustrates the calculation steps performed by `husimi.farfield.farfield_from_husimi`.

![farfield calculation steps](readme_images/farfield_from_husimi.png){width=600}

- Upper left: outer emerging Husimi function $H_0^\mathrm{em}(s, \sin \chi)$.
- Upper right: The background shows the Husimi rescaled to show the dependence on the angle $\chi$ of the emerging wave. The red crosses denote the points where the Husimi function is sampled.
- Lower left: The black lines show the boundary of the cavity. The red dots denote the arc length samples. The protruding gray lines show the sampled angles (angle to the surface normal vector) and radiation intensity (length).
- Lower right: The intensities are summed up at each angle resulting in the shown farfield. Due to the low samplesize ($20 \times 20$ points in phase space were sampled) the curve is pretty rugged.

Experience has shown that for a sample size of $N^2$ points in phase space, $N$ farfield angles should be used. Otherwise the farfield is either dominated by noise or the bins are so large, that the farfield becomes trivial.

For large sample sizes and a Husimi function of high resolution, a good approximation can be found:

![farfield comparison](readme_images/husimi_farfield.png){width=400}

## Notes on COMSOL

As COMSOL is used to obtain the data in the demo, the following contains a few words on how to utilize COMSOL to obtain mode data.

### Complex-valued mode

By default, COMSOL's frequency domain calculations give complex-valued mode patterns. This lies in the fact, that the system is open. The field is thus defined as

```math
E_z(\vec{r}, t) = E^0_z(\vec{r}) \mathrm{e}^{\mathrm{i} \omega t}
```

with $\omega = 2 \pi f \in \mathbb{C}$, where $E^0_z(\vec{r})$ is the mode acquired through COMSOL.

![farfield comparison](readme_images/field_RealComplexParts.png){width=600}

The image above shows the real and complex parts of the demo mode. The absolute value of the complex part insie the cavity is significantly lower than that of the real part. It still has a significant impact on the field outside the cavity. This can be seen in the mode image below. Even though the cavity radiates e.g. to the towards the $\varphi = 0$ direction of the cavity, there are now nodes visible in the absolute value of the field. This is due to the waves travelling away from the cavity with $E(x) = E_0 \exp \left(\mathrm{i} kx \right)$ (due to the Helmholtz-equation). As $|E(x)| = E_0 |\exp \left(\mathrm{i} kx \right)| = E_0 |\cos \left(kx \right) + \mathrm{i} \, \sin \left(kx \right)| = E_0$ holds, the intensity is constant.

![farfield comparison](readme_images/field_root.png){width=400}

### Obtaining the boundary function and its radial derivative

The field function can be obtained in COMSOL by using a _Global Parametric Sweep_ (GPS). First we define functions giving the boundary coordinates depending on the angle $\varphi$. In the following $\varphi = 0$ points upwards, giving e.g.

```math
\vec{r}(\varphi) 
= \begin{pmatrix}x(\varphi) \\ y(\varphi) \end{pmatrix}
= R(\varphi) \begin{pmatrix}- \sin(\varphi) \\ \cos(\varphi) \end{pmatrix}
\quad \text{with} R(\varphi) = R_0 (1 + \varepsilon \cos \varphi) 
```

for parametrizing the curve.

The position on the boundary $s(\varphi)$ is also important, which can be obtained by evaluating the integral

```math
s(\varphi) = \int_0^\varphi \sqrt{R^2(x) + R'^2(x)} \mathrm{d} x \quad ,
```

which cannot be done analytically. Fortinately COMSOL can integrate numerically with `integrate(sqrt(R(x)^2 + Rdash(x)^2), x, 0, phi)`

Next we obtain $E_z$ on the boundary. This is done using the built-in function `at( x(phi), y(phi), ewfd.Ez )`, which gives $E_z$ at $(x,y)$ with $x, y$ being points on the boundary.

For the derivative, we need to use the built-in derivation function with `d(ewfd.Ez, x)`, which gives the derivative in $x$-direction. The $y$ direction is obtained similarly. Only the derivative in $x,y$ directions is available, so we need to multiply the gradiant with the surface normal vector $\vec{n}(s)$. This gives

```math
\Psi'_j(s) = \vec{n}_j(s) \cdot \vec{\nabla} E_z(s)
```

with $\vec{n}_0(s)$ pointing inwards, $\vec{n}_1(s)$ outwards the cavity for the inner/outer derivatives. This gives `at( x(phi), y(phi), ( (Rdash * cos(phi) - R * sin(phi)) * d(ewfd.Ez, x) + (Rdash * sin(phi) + R * cos(phi)) * d(ewfd.Ez, x) ) / sqrt(R^2 + Rdash^2)  )`

In summary we use the following columns in the GPS:

| Parameter | mathematical expression | COMSOL code |
| :---: | --- | --- |
| $\varphi$  | $\varphi$ | `phi` |
| $s(\varphi)$ | $\int_0^\varphi \sqrt{R^2(x) + R'^2(x)} \mathrm{d} x$ | `integrate( sqrt(r(x)^2 + dr(x)^2), x, 0, phi )` |
| $x(\varphi)$ | $x(\varphi)$ | `x(phi)` |
| $y(\varphi)$ | $y(\varphi)$ | `y(phi)` |
| $\Psi_j(\varphi)$ | $E_z(\varphi)$ | `at( x(phi), y(phi), ewfd.Ez )` |
| $\Psi'_j(\varphi)$ | $\vec{n}_j(\varphi) \cdot \vec{\nabla} E_z(\varphi)$  | `at( x(phi), y(phi), ( (Rdash * cos(phi) - R * sin(phi)) * d(ewfd.Ez, x) + (Rdash * sin(phi) + R * cos(phi)) * d(ewfd.Ez, x) ) / sqrt(R^2 + Rdash^2)  )` |

### Proof for the COMSOL expressions

#### Arclength

The arclength $s(\varphi)$ can be calculated by integrating the parameteric curve defining the microcavity boundary.

```math
\begin{aligned}
s(\varphi) &= \int_\Gamma |\mathrm{d}\vec{r}(\varphi)| = \int_\Gamma \left| \frac{\partial\vec{r}(\varphi)}{\partial \varphi}  \right| \mathrm{d}\varphi
\\&= \int_0^\varphi \left| \frac{\partial}{\partial \varphi} R(\varphi) \begin{pmatrix}
- \sin(\varphi) \\
\cos(\varphi)
\end{pmatrix} \right| \mathrm{d}\varphi
\\&= \int_0^\varphi \left| \begin{pmatrix}
- R'(\varphi) \sin(\varphi) - R(\varphi) \cos(\varphi) \\
R'(\varphi) \cos(\varphi) - R(\varphi) \sin(\varphi)
\end{pmatrix} \right| \mathrm{d}\varphi
\\&= \int_0^\varphi
\sqrt{R'^2(\varphi) + R^2(\varphi)}
\mathrm{d}\varphi
\end{aligned}
```

### Normal Vector and derivative

To obtain the derivative boundary function, we need to evaluate

```math
\Psi'_j(\varphi) = \vec{n}_j(\varphi) \cdot \vec{\nabla} E_z(\varphi) \quad ,
```

so the first step is to obtain an expression for $\vec{n}_j(\varphi)$. The normal vector is defined as

```math
\vec{n}_j(\varphi) = \frac{(-1)^j}{|\vec{t}(\varphi)|} \begin{pmatrix} - t_y(\varphi) \\ t_x(\varphi) \end{pmatrix}
```

with the tangent vector $\vec{t}(\varphi)$. It can be written as

```math
\vec{t}(\varphi) = \frac{\partial}{\partial \varphi} \vec{r}(\varphi) =
\begin{pmatrix}
- R'(\varphi) \sin \varphi - R(\varphi) \cos \varphi \\
- R'(\varphi) \cos \varphi + R(\varphi) \sin \varphi
\end{pmatrix} \quad .
```

When substituted in the expression for $\vec{n}_j(\varphi)$, we arrive at

```math
\vec{n}_j(\varphi) = \frac{(-1)^j}{\sqrt{R'^2(\varphi) + R^2(\varphi)}} 
    \begin{pmatrix}
        - R'(\varphi) \sin \varphi - R(\varphi) \cos \varphi \\ 
        R'(\varphi) \cos \varphi - R(\varphi) \sin \varphi
    \end{pmatrix} \quad .
```

With this, the derivative boundary function can be evaluated.
