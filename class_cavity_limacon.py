"""Useful functions for limacon treatment
"""

import numpy as np
from scipy.integrate import quad
from typing import Tuple

from .class_cavity import Cavity

class Limacon(Cavity):
    def __init__(
            self,
            R0:"float"=1.,
            deform:"float"=0.4,
            xpos:"float" = 0.,
            ypos:"float" = -0.4,
            phi0:"float" = 0.0
            ) -> None:
        
        super().__init__(R0, xpos, ypos)

        self.deform:"float" = deform # deformation parameter
        self.phi0:"float" = phi0     # rotation angle
        
        return None
    

    ###########################################
    # base definitions
    ###########################################

    def radius(self, phi:"float") -> "float":
        return self.R0 * (1. + self.deform * np.cos(phi))


    def radiusDerivative(self, phi:"float") -> "float":
        return - self.R0 * self.deform * np.sin(phi)
