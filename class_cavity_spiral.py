"""Useful functions for limacon treatment
"""

import numpy as np
from scipy.integrate import quad
from typing import Tuple

from .class_cavity import Cavity

class Spiral(Cavity):
    def __init__(
            self,
            R0:"float"=1.,
            deform:"float" = 0.1,
            xpos:"float" = 0.,
            ypos:"float" = 0.0,
            phi0:"float" = 0.0
            ) -> None:
        """Initialize a Spiral"""
        
        super().__init__(R0, xpos, ypos)
        
        self.deform = deform
        self.phi0 = phi0
        
        return None
    



    ###########################################
    # base definitions
    ###########################################

    def radius(self, phi:"float") -> "float":
        return self.R0 * (1. - self.deform * phi *.5 / np.pi)


    
    def radiusDerivative(self, phi:"float"=0.) -> "float":
        return - self.R0 * self.deform * .5 / np.pi
