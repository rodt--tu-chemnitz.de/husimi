"""The ParameterSet class contains all the helper functions, which can be used to
obtain the system parameters from the filename.

The old file is from the helper.py file.
"""

import json
from scipy.constants import speed_of_light as SPEED_OF_LIGHT # all caps because its a constant
import numpy as np
import os

from typing import Tuple

from .file import read_comsol_file

class ParameterSet():
    def __init__(self,
        fileName:"str" = None,
        modeDir:"str" = None
        ):

        if (fileName is None) and (modeDir is None):
            print('ERROR: either file or modedir must be set')
            raise ValueError
        # filename has higher priority
        elif fileName is not None:
            self.fileName:"str" = fileName
            self.modeDir:"str" = self.get_modeDir()
            
            # mode directory should always be defined
            # this should not be accessed directly, but by properties
            self._modeParams:"dict" = self._read_modeParamJSON()
            
            self.complexfreqs, self.qualityfactors = self.read_eigenvalues()
            self.minQ:"float" = 1e3 # minimum quality factor
            
            # read eigenvalues
            self.eigval_Hz_complex:"list[complex]"
            self.eigval_Qfactors:"list[complex]"
            self.eigval_Hz_complex, self.eigval_Qfactors = self.read_eigenvalues()
            
            # read parities
            self.parities:"list[int]|None" = self.read_parities()
            
            
        # only when really necessary is only mode directory saved
        else:
            self.fileName:"str" = None
            self.modeDir:"str" = modeDir
        
            # mode directory should always be defined
            # this should not be accessed directly, but by properties
            self._modeParams:"dict" = self._read_modeParamJSON()


        return



    ##########################
    # init functions
    ##########################

    def get_modeDir(self) -> "str":
        """Returns the mode directory from a given filename

        Returns:
            str: Mode directory
        """
        modeDir:"str" = os.path.dirname(os.path.dirname(self.fileName))
        return modeDir



    def _read_modeParamJSON(self, fname:"str"='modeparams.json') -> "dict":
        """Reads mode parameter json

        Args:
            fname (str, optional): Filename. Defaults to 'modeparams.json'.

        Returns:
            dict: Dictionary containing mode parameters, such as frequency and wavelength
        """
        modeParams_fileName_complete:"str" = '{0}/{1}'.format(self.modeDir, fname)
        
        with open(modeParams_fileName_complete, 'r') as f:
            modeParams = json.load(f)
        
        return modeParams

    
    ##########################
    # eigenvalue functions -> important, in case more than 4 modes are calculated
    ##########################


    def read_eigenvalues(self) -> "Tuple[list[complex], list[float]]":
        """Read eigenfrequencies corresponding to filename

        Returns:
            list[complex]: list of eigenvalues
        """
        fileName_suffix = self._fileNameSplit[-1].split('_')[-1]
        eigenvalue_fname:"str" = '{0}/frequencies/frequencies_cavitydist_{1}'.format(self.modeDir, fileName_suffix)
        
        # maybe eigenvalues are not there
        try: eigenvalues:"list[float]" = np.array( read_comsol_file(eigenvalue_fname, skip_header=9) ).flatten()
        except FileNotFoundError: return None, None
        
        eigenvalues = eigenvalues[2:] # cut away the two coord columns
        n_values = len(eigenvalues)
        
        # seperating data
        freq_THz_real_list:"list[float]" = np.array( [eigenvalues[ii*2] for ii in range(n_values//2)] )
        Q_factor_list:"list[float]" = np.array( [eigenvalues[ii*2+1] for ii in range(n_values//2)] )
        
        # building complex frequencies
        freq_THz_imag_list:"list[float]" = freq_THz_real_list / Q_factor_list * .5
        freq_TH_complex_list:"list[complex]" = freq_THz_real_list + 1j * freq_THz_imag_list
        
        return freq_TH_complex_list, Q_factor_list

    
    
    def get_eigenvalue_selector(self,
            Qmin:"float"=700            # value is arbitrarily chosen, so that it gives the modes that we want
            ) -> "list[bool]":
        """Returns bool list of eigenvalues larger than Qmin

        Args:
            Qmin (float, optional): Minimum Q factor. Defaults to 1e3.

        Returns:
            list[bool]: List if quality factor of data is high enough
        """
        return self.eigval_Qfactors > Qmin



    def read_parities(self) -> "list[int] | None":
        """Reads the parities if able, otherwise returns None.

        Returns:
            list[int]: List of parities, if able
        """
        filename_parity:"str" = '{0}/parities/parities_dist-per-wavelen={1:.6f}.dat'.format(self.resultsDir, self.cavityDist_per_wavelength)
        
        try: # try reading it
            parity_list:"list[int]" = np.loadtxt(filename_parity, dtype=int)
        except FileNotFoundError: # if not found (e.g. when parities are not yet calculated)
            parity_list:"None" = None
        
        return parity_list



    ##########################
    # directory properties
    ##########################

    @property
    def _fileNameSplit(self) -> "list[str]":
        """Splits filename along slashes.

        Returns:
            list[str]: filename split.
        """
        return self.fileName.split('/')
    


    @property
    def _modeDirSplit(self) -> "list[str]":
        """Splits filename along slashes.

        Returns:
            list[str]: filename split.
        """
        return self.modeDir.split('/')



    @property
    def modeName(self) -> "str":
        """Returns the mode name from a given filename

        Returns:
            str: Mode directory
        """
        modeName:"str" = self._modeDirSplit[1]
        return modeName
    


    @property
    def _modeNameSplit(self) -> "list[str]":
        """Splits 

        Returns:
            list[str]: _description_
        """
        return self.modeName.split('_')



    @property
    def resultsDir(self) -> "str":
        """Returns the mode directory for results 

        Returns:
            str: results directory
        """
        resultsDir:"str" = 'results/{0}'.format(self.modeName)
        return resultsDir
    

    ##########################
    # properties arising from directory or file names
    ##########################

    @property
    def n_cavities(self) -> "int":
        n_cavities:"int" = int( self._modeNameSplit[2] )
        return n_cavities



    @property
    def n_modes(self) -> "int":
        return 2 * self.n_cavities



    @property
    def cavityDist_m(self) -> "float":
        """Gives the distance for filenames of COMSOL sweeps
        Returns:
            float: Distance in m
        """

        # remove ending
        fname_noSuffix = self._fileNameSplit[-1][:-4]
        
        # get distance
        dist_str:"str" = fname_noSuffix.split('_')[-1]
        dist_m:"float" = float( dist_str )
        
        # all done
        return dist_m
    
    @property
    def cavityDist_um(self) -> "float":
        """Gives the distance for filenames of COMSOL sweeps
        Returns:
            float: Distance in um
        """
        return self.cavityDist_m * 1e6
    
    @property
    def cavityDist_per_wavelength(self) -> "float":
        """Gives distance in terms of D/lambda

        Returns:
            float: D/lambda
        """
        return self.cavityDist_m / self.wavelength_m


    ##########################
    # parameter properties
    ##########################

    # accessing the dict

    @property
    def _freq_THz_str(self) -> "str":
        return self._modeParams['freq_THz']


    @property
    def refractiveIndex(self) -> "float":
        return self._modeParams['refractiveIndex']
    


    @property
    def rotationAngles_2pi(self) -> "list[float]":
        try:
            rotationAngles_2pi:"list[float]" = np.array( self._modeParams['rotationAngles_2pi'] )
        except KeyError: # if key doesn't exist, no rotation is assumed
            rotationAngles_2pi:"list[float]" = np.zeros(self.n_cavities)
        
        return rotationAngles_2pi
    
    @property
    def rotationAngles_rad(self) -> "list[float]":
        return self.rotationAngles_2pi * 2.*np.pi



    @property
    def Reff_um(self) -> "float":
        """Reads the effective radius of the mode

        Returns:
            float: Effective Radius in um.
        """
        return self._modeParams['effective_radius']
    
    @property
    def Reff_m(self) -> "float":
        return self.Reff_um * 1e-6

    
    
    @property
    def deform(self) -> "float":
        """Deformation parameter of limacon

        Returns:
            float: deformation
        """
        deform:"float"
        deformKey:"str" = 'deform'
        
        # check if deformation parameter is defined
        # if it is, read it
        if deformKey in self._modeParams.keys():
            deform = self._modeParams[deformKey]
        # if it is not, use 0.4 for backwards compatibility
        else:
            deform = 0.4

        return deform
    
    
    @property
    def deform_list(self) -> "list[float]":
        """Deformations of all cavities in array

        Returns:
            list[float]: deformations
        """
        return np.ones(self.n_cavities) * self.deform
    


    # processing the dict

    @property
    def freq_THz(self) -> "complex":
        return complex( self._freq_THz_str )
    
    @property
    def freq_Hz(self) -> "complex":
        return self.freq_THz * 1e12


    # wavelengths
    
    @property
    def wavelength_m_complex(self) -> "complex":
        """Reads the complex wavelength of the mode

        Returns:
            float: Wavelength in m.
        """
        wavelength_m:"complex" = SPEED_OF_LIGHT / self.freq_Hz
        return wavelength_m

    @property
    def wavelength_m(self) -> "float":
        """Reads the wavelength of the mode

        Returns:
            float: Wavelength in m.
        """
        return self.wavelength_m_complex.real
    
    @property
    def wavelength_um(self) -> "float":
        """Reads the wavelength of the mode
        Returns:
            float: Wavelength in um.
        """
        return self.wavelength_m * 1e6
    
    
    # wavenumbers
    
    @property
    def wavenumber_per_m_complex(self) -> "complex":
        """Reads the wavenumber of mode

        Returns:
            float: Wavenumber k in 1/m
        """
        return 2.*np.pi / self.wavelength_m_complex
    
    @property
    def wavenumber_per_m(self) -> "float":
        """Reads the wavenumber of mode

        Returns:
            float: Wavenumber k in 1/m
        """
        return self.wavenumber_per_m_complex.real

    @property
    def wavenumber_per_um_complex(self) -> "complex":
        """Reads the wavenumber of mode

        Returns:
            float: Wavenumber k in 1/um
        """
        return self.wavenumber_per_m_complex * 1e-6
    
    @property
    def wavenumber_per_um(self) -> "float":
        """Reads the wavenumber of mode

        Returns:
            float: Wavenumber k in 1/um
        """
        return self.wavenumber_per_um_complex.real
    
    
    # unitless wavenumber
    
    @property
    def nkR_complex(self) -> "complex":
        """Unitless frequency nkR

        Returns:
            float: nkR
        """
        return self.refractiveIndex * self.wavenumber_per_um_complex # R0=1um, so is omitted
    
    @property
    def nkR(self) -> "float":
        """Unitless frequency nkR

        Returns:
            float: nkR
        """
        return self.nkR_complex.real
    
    @property
    def eigval_nkR(self) -> "list[complex]":
        return 2.*np.pi * self.eigval_Hz_complex / SPEED_OF_LIGHT * self.refractiveIndex * 1e-6

    
    # center distances

    @property
    def centerDist_um(self) -> "float":
        return (2. * self.Reff_um)  + self.cavityDist_um
    
    @property
    def centerDist_m(self) -> "float":
        return self.centerDist_um * 1e-6
    
    
    
    ##########################
    # miscellaneous properties
    ##########################

    @property
    def points_per_wavelength(self) -> "int":
        """Number of distances per wavelength."""
        return int( self._modeParams['points_per_wavelen'] )
