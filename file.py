'''
Functions for husimi saving and reading.
'''

from typing import Tuple, Any

import numpy as np
import pandas as pd





################################################################
# saving and reading functions
################################################################

def save_husimi_to_csv(
        q_list: "list[float]",
        p_list: "list[float]",
        data: "list[list[float]]",
        fname: "str" = 'husimi.csv'
        ) -> None:
    '''
    Saves the husimi function to a csv. The columns denote the arclength, the rows the impulse.
    '''
    
    # columnnamnes are q
    columns = np.concatenate(([0.], q_list))
    columns_str = ['{0:.4f}'.format(x) for x in columns] # round values to omit numerical noise
    if len(q_list) > 1e4:
        raise ValueError('Error: {0} are too many q_list values, file columns are deprecated!'.format(q_list))
    
    # rearrange data, so that impulse list is first column
    # with stack we rebuild the entire array
    data_with_impulses = np.stack((p_list, *data.T), axis=1)
    
    
    # create dataframe
    df = pd.DataFrame(
        data=data_with_impulses,
        columns=columns_str
        )
    
    #
    df.to_csv(
        fname,
        index=False
        )
    
    return None



def read_husimi_csv(fname:"str") -> "Tuple[list[float], list[float], list[list[float]]]":
    '''
    reads a husimi file
    '''


    df = pd.read_csv(fname, sep=',')


    s_list: "list[str]" = list(df.keys())
    s_list = ['0.0000'] + s_list[2:]
    # s_list[1] = s_list[1][2:] # remove the superfluous 0.
    s_list: "list[float]" = np.array(s_list, dtype=float)

    p_list: list[float] = df['0.0000'].values


    husimi: "list[list[float]]" = df.values[:,1:]

    return s_list, p_list, husimi



def read_husimi_csv_joined(fname_list:"list[str]") -> "Tuple[list[float], list[float], list[list[float]]]":
    """Reads list of Husimi function names and returns all data in one dataset

    Args:
        fname_list (list[str]): List of Husimi filenames

    Returns:
        Tuple[list[float], list[float], list[list[float]]]: q, p, husimi
    """
    
    # initialize
    q_listlist:"list[list[float]]" = []
    p_listlist:"list[list[float]]" = []
    data_list:"list[list[float]]" = []
    
    # reading data
    for fn in fname_list:
        q_list, p_list, husimiData = read_husimi_csv(fn)
        
        q_listlist.append(q_list)
        p_listlist.append(p_list)
        data_list.append(husimiData)

    # preparing data
    q_listlist = np.array(q_listlist)
    p_listlist = np.array(p_listlist)
    data_list = np.array(data_list)
    
    # joining data, last element in q is neglected (otherwise there would be 2 values for one q)
    q_list_spliced:"list[float]" = np.concatenate([cavityIndex + line[:-1] for cavityIndex, line in enumerate(q_listlist)])
    p_list_spliced:"list[float]" = p_listlist[0] # the same for all plots
    data_spliced:"list[list[float]]" = np.hstack(data_list[:,:,:-1])
    
    
    return q_list_spliced, p_list_spliced, data_spliced



################################################################
# comsol functionalities
################################################################

def complex_number_converter(s:str) -> complex:
    '''
    Takes complex number like "1.+2.i" (using i as complex number)
    and returns "1.+2.j" (as complex number).
    '''
    # print(s, s.replace('i', 'j'), complex(s.replace('i', 'j')))
    return complex(s.replace('i', 'j'))



def read_comsol_file(    
        fname:"str", 
        print_info:"bool"=False,
        skip_header:"int" = 5
        ) -> "list[list[Any]]":
    '''
    Reads a general COMSOL file and returns header and data as two seperate lists 
    '''

    if print_info:
        print('reading comsole file {0}'.format(fname))

    # read contents
    with open(fname, 'r') as f:
        lines = f.readlines()

    # read headers

    if print_info:
        print('- model info')
        for ii in range(skip_header):
            # first to symbols are 
            print('\t', lines[ii][2:-1])

    
    if print_info:
        print('- isolating data')
    
    n_cols = len(lines[skip_header].split())
    data_all = [[] for _ in range(n_cols)]

    
    if print_info: print('\t-> {0} columns found'.format(n_cols))
    
    
    for line in lines[skip_header:]:
        for ii, linePart in enumerate(line.split()):
            # check if the numper is real or complex
            if 'i' in linePart:
                try:
                    val = complex_number_converter(linePart)
                except ValueError:
                    val = linePart
            else:
                val = float(linePart)
            
            data_all[ii].append(val)

  
    # turn to numpy array
    # should work, as each column should have the same data type
    for ii in range(n_cols):
        data_all[ii] = np.array(data_all[ii])

    return data_all




def split_resonators(cavity_index:"list[int]", *args:"list[list[Any]]") -> "list[list[Any]]":
    """Split comsol file data per cavity

    Args:
        cavity_index (list[int]): List of indizes, which indicates which datapoint belongs to what.

    Returns:
        list[list[Any]]: Array of split data without cavity index list
    """

    
    # obtain cavity switch positions
    cavity_index_delta:"list[int]" = cavity_index[1:] - cavity_index[:-1]
    cavity_index_switch:"list[bool]" = 1e-3 < cavity_index_delta
    switchpos = np.concatenate(( [0], np.nonzero(cavity_index_switch)[0] + 1 ))  # indizes of cavity data startpoints
    
    

    n_cavities:"int" = len(switchpos)

    
    # preparing split args
    args_split:"list[list[Any]]" = []
    
    for line in args:
        split_now = []

        # beginning splits
        for ii in range(n_cavities-1):
            startIndex = switchpos[ii]
            endIndex = switchpos[ii+1]
            split_now.append( line[startIndex:endIndex] )
        
        # end split
        startIndex = switchpos[-1]
        split_now.append( line[startIndex:] )
        
        
        # saving
        args_split.append( np.array(split_now) )

    
    return args_split



def read_comsol_file_split(fname:"str", 
        **kwargs
        ) -> "list[list[Any]]":
    """Reads comsol file and splits it along resonators

    Args:
        fname (str): Filename

    Returns:
        list[list[Any]]: Data from file.
    """
    return split_resonators(
            *read_comsol_file(
            fname, 
            **kwargs
            )
        )



################################################################
# progress bar
################################################################

def printProgressBar (iteration, total, prefix = 'Progress:', suffix = 'complete', decimals = 1, length = 20, fill = '█', printEnd = "\r"):
    """Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    
    Sample usage:
    
    import time

    # A List of Items
    items = list(range(0, 57))
    l = len(items)

    # Initial call to print 0% progress
    printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
    for i, item in enumerate(items):
        # Do stuff...
        time.sleep(0.1)
        # Update Progress Bar
        printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
    
    Credit: Greenstick in https://stackoverflow.com/questions/3173320/text-progress-bar-in-terminal-with-block-characters
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Print New Line on Complete
    if iteration == total: 
        print()