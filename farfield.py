'''
Contains functions for farfield calculation from husimi data
'''


from typing import Tuple

import numpy as np
from scipy.interpolate import interpn, CubicSpline
from scipy.integrate import trapezoid

import matplotlib.pyplot as plt

from .plot import plot_husimi


def farfield_from_husimi(
        q_list:"list[float]",
        p_list:"list[float]",
        husimiData:"list[float]",
        arcLength_list:"list[float]",
        phi_list:"list[float]",
        deform:"float"=0.4,
        n_interpolationPoints_s:"int" = 200,
        n_interpolationPoints_chi:"int" = 300,
        n_farfield_angles:"int" = 200,
        intensityFunc:"function" = np.sqrt,
        intensityFunc_inverse:"function" = np.square,
        plotting:"bool" = False,
        boundaryX:"list[float]|None" = None,
        boundaryY:"list[float]|None" = None,
        fname:"str" = 'farfield_from_husimi.png',
        modeData:"Tuple[list[float], list[float], list[list[float]]]" = [[], [], [[]]]
        ) -> "Tuple[list[float], list[float]]":
    """Calculates the farfield from the outer emerging Husimi function.

    Args:
        q_list (list[float]): Unitless spatial coordinates.
        p_list (list[float]): Unitless momentum coordinates.
        husimiData (list[float]): Outer emerging Husimi data.
        arcLength_list (list[float]): Arclengths on boundary
        phi_list (list[float]): Angles corresponding to arclengths
        deform (float): Deformation parameter of limacon. Defaults to 0.4.
        n_interpolationPoints_s (int, optional): Number of points for spatial coordinates in which the Husimi is interpolated. Defaults to 200.
        n_interpolationPoints_chi (int, optional): Number of points for momentum coordinates in which the Husimi is interpolated. Defaults to 300.
        n_farfield_angles (int, optional): Farfield angles. Defaults to 200.
        intensityFunc (function, optional): Function to be applied to husimi function for farfield calculation. Defaults to np.sqrt.
        intensityFunc_inverse (function, optional): Function to be applied to summed intensities for farfield calculation. Defaults to np.square.
        plotting (bool, optional): Enables plotting for diagnostic purposes. Defaults to False.
        boundaryX (list[float]|None, optional): X-coordinates of cavity boundary. Only needed when plotting. Defaults to None.
        boundaryY (list[float]|None, optional): Y-coordinates of cavity boundary. Only needed when plotting. Defaults to None.
        fname (str, optional): Filename for diagnostic plot. Defaults to 'farfield_from_husimi.png'.
        modeData: mode pattern for plotting

    Returns:
        Tuple[list[float], list[float]]: Angles and farfield intensities.
    """
    
    # calculate angles
    chi_list:"list[float]" = np.arcsin(p_list) / (np.pi * .5)


    ################################################
    # interpolating husimi function
    ################################################

    # interpolation s and chi coordinates
    interpolation_s:"list[float]" = np.arange(0,n_interpolationPoints_s) / n_interpolationPoints_s
    interpolation_chi:"list[float]" = np.linspace(-1,1,n_interpolationPoints_chi)

    interpolation_evalPoints_chi,  interpolation_evalPoints_s = np.meshgrid(interpolation_chi, interpolation_s)


    # evaluation positions    
    interpolation_evalPoints_vectors:"list[Tuple[float,float]]" = np.transpose([
        interpolation_evalPoints_s.flatten(),
        interpolation_evalPoints_chi.flatten()
        ])
    

    # create interpolated grid
    radiationIntensity:"list[float]" = interpn(
        [q_list, chi_list],
        husimiData.T,
        interpolation_evalPoints_vectors,
        bounds_error=False,
        fill_value=.0
        )
    radiationIntensity_normalized:"list[float]" = radiationIntensity / np.max(radiationIntensity)
    husimiContributions:"list[float]" = intensityFunc(radiationIntensity_normalized)
    


    # obtain boundary position of selected s values
    interpolationFunc_phi_of_s = CubicSpline(arcLength_list, phi_list)

    if plotting:
        interpolationFunc_x_of_s = CubicSpline(arcLength_list, boundaryX)
        interpolationFunc_y_of_s = CubicSpline(arcLength_list, boundaryY)


    ################################################
    # analyzing emerging husimi
    ################################################

    if plotting:
        ray_list:"list[Tuple[list[float], list[float]]]" = []

    
    # calculate far field
    angle_intensity_list:"list[Tuple[float,float]]" = []
    for (ss, chi), contribution_now in zip(interpolation_evalPoints_vectors, husimiContributions):
        # use field strength for farfield purposes
        
        phi:"float" = interpolationFunc_phi_of_s(ss)
        normalVector_now:"Tuple[float, float]" = normalvector(phi, deform=deform)

        # calculating angle and intensity in that direction
        intensityVector:"Tuple[float, float]" = normalVector_now @ rotMat(chi * np.pi * .5)
        intensityVector_angle:"float" = np.angle(intensityVector[0] + 1j * intensityVector[1])
        angle_intensity_list.append([intensityVector_angle, contribution_now])


        if plotting:
            ray_start:"Tuple[float, float]" = np.array([interpolationFunc_x_of_s(ss), interpolationFunc_y_of_s(ss)])
            ray_end:"Tuple[float, float]" = ray_start + intensityVector  * contribution_now
            ray_list.append(np.transpose([ray_start, ray_end]))



    ################################################
    # analye
    ################################################

    husimi_angles, husimi_weights = np.transpose(angle_intensity_list)


    farfield_angle_delta = 2. * np.pi / n_farfield_angles
    

    farfield_bin_edges = np.linspace(
        - np.pi - farfield_angle_delta * .5,
        + np.pi + farfield_angle_delta * .5,
        n_farfield_angles + 2
        )

    
    farfield_angles = (farfield_bin_edges[1:] + farfield_bin_edges[:-1]) * .5


    
    farfield_hist_weighted, _ = np.histogram(
        husimi_angles,
        bins=farfield_bin_edges,
        weights=husimi_weights
        )

    

    # last and first value need to be added, as the scan the same region
    edgeSum = farfield_hist_weighted[0] + farfield_hist_weighted[-1]
    farfield_hist_weighted[0] = edgeSum
    farfield_hist_weighted[-1] = edgeSum

    # taking the square
    # root intensity corresponding to field sums up
    # -> in final step summed "fields" are squared
    farfield_hist_weighted = intensityFunc_inverse(farfield_hist_weighted)

    # norming the data
    farfield_normed = farfield_hist_weighted / np.max(farfield_hist_weighted)



    ################################################
    # plotting
    ################################################

    if plotting:
        fig, axes = plt.subplots(
            ncols=2, nrows=2,
            figsize = (8,6)
            )


        # husimi normal

        ax_husimi_normal = axes[0,0]

        plot_husimi(
            q_list, p_list, 
            intensityFunc(husimiData), 
            ax_use=ax_husimi_normal,
            cmap='viridis'
            )
        

        # husimi chi

        ax_husimi_chi = axes[0,1]

        ax_husimi_chi.contourf(
            q_list, chi_list, 
            intensityFunc(husimiData)
            )
        
        ax_husimi_chi.set_ylabel(r'$\chi$')

        ax_husimi_chi.plot(
            *interpolation_evalPoints_vectors.T,
            'ro ',
            alpha=.2
            )
        
        ax_husimi_chi.set_ylim(-1,1)


        
        # boundary

        ax_boundary = axes[1,0]
        
        # mode_xCoords, mode_yCoords, mode_field = modeData
        # mode_field = np.abs(mode_field)**2
        
        # ax_boundary.tricontourf(
        #     mode_xCoords,
        #     mode_yCoords,
        #     mode_field,
        #     cmap='Blues',
        #     vmin=0
        #     )
        

        ax_boundary.plot(
            boundaryX, boundaryY,
            'k-',
            zorder=4
            )
        
        ax_boundary.plot(
            interpolationFunc_x_of_s(interpolation_evalPoints_s),
            interpolationFunc_y_of_s(interpolation_evalPoints_s),
            'ro ', 
            zorder=5
            )
        

        for ray in ray_list:
            ax_boundary.plot(
                *ray,
                color='gray',
                zorder=1
                )

        ax_boundary.set_aspect('equal')
        ax_boundary.set_xlim(-1.5,1.5)
        ax_boundary.set_ylim(-1.5,1.5)


        # farfield

        ax_farfield = axes[1,1]

        ax_farfield.plot(
            farfield_angles/np.pi,
            farfield_normed,
            zorder=3
            )
        
        ax_farfield.set_xlabel(r'$\theta$ [$\pi$]')
        ax_farfield.set_ylabel(r'farfield intensity')
        
        ax_farfield.set_xlim(-1,1)
        ax_farfield.set_ylim(0,1)
        
        for x in [-.5,.5]:
            ax_farfield.axvline(
                x,
                color='k',
                ls='--',
                linewidth=1.
                )


        # saving

        fig.tight_layout()
        fig.savefig(fname, bbox_inches='tight', dpi=300)
        plt.close(fig)

    ################################################
    # done
    ################################################


    return np.array([farfield_angles, farfield_normed])




def vector_angle(vec:"Tuple[float,float]") -> "float":
    """Returns angle of vector to (1,0) in mathematically positive definition

    Args:
        vec (Tuple[float,float]): Vector

    Returns:
        float: angle
    """
    return np.angle(vec[0] + 1j * vec[1])



##############################################
# farfield norms
##############################################

def integral_norm(angles:"list[float]", farfield:"list[float]") -> "list[float]":
    """Norms the farfield by making the integral equatio to one

    Args:
        angles (list[float]): Farfield angles.
        farfield (list[float]): Farfield intensities.

    Returns:
        list[float]: Normed farfield.
    """
    integral:"float" = trapezoid(farfield, angles)
    return farfield / integral
    


##############################################
# small functions
##############################################


def lima_radius(
        phi:"float",
        R0:"float"=1.,
        deform:"float"=0.4,
        ) -> "float":
    """Returns limacon radius.

    Args:
        phi (float): Angle, phi=0 is at maximum radius.
        R0 (float, optional): Radius of corresponding disc. Defaults to 1..
        deform (float, optional): Deformation parameter. Defaults to 0.4.

    Returns:
        float: Radius of Limacon
    """
    return R0 * (1. + deform * np.cos(phi))



def lima_radiusDerivative(
        phi:"float",
        R0:"float"=1.,
        deform:"float"=0.4,
        ) -> "float":
    """Returns limacon radius derivative.

    Args:
        phi (float): Angle, phi=0 is at maximum radius.
        R0 (float, optional): Radius of corresponding disc. Defaults to 1..
        deform (float, optional): Deformation parameter. Defaults to 0.4.

    Returns:
        float: Radius derivative of Limacon
    """
    return - R0 * deform * np.sin(phi)



def tangent(phi:"float", deform:"float"=0.4) -> "Tuple[float, float]":
    """Returns normed tangent vector at limacon boundary.

    Args:
        phi (float): Angle

    Returns:
        Tuple[float, float]: Tangent vector
    """
    
    r = lima_radius(phi, deform=deform)
    rDash = lima_radiusDerivative(phi, deform=deform)

    xcomponent = - rDash * np.sin(phi) - r * np.cos(phi)
    ycomponent = + rDash * np.cos(phi) - r * np.sin(phi)

    normfac = 1. / np.sqrt(r**2 + rDash**2)

    return normfac * np.array([xcomponent, ycomponent])



def normalvector(phi:"float", deform:"float"=0.4) -> "Tuple[float, float]":
    """Returns normal vector pointing outward the cavity.

    Args:
        phi (float): Angle

    Returns:
        Tuple[float, float]: Normal vector
    """
    
    r = lima_radius(phi, deform=deform)
    rDash = lima_radiusDerivative(phi, deform=deform)

    xcomponent = + rDash * np.cos(phi) - r * np.sin(phi)
    ycomponent = + rDash * np.sin(phi) + r * np.cos(phi)

    normfac = 1. / np.sqrt(r**2 + rDash**2)

    return + normfac * np.array([xcomponent, ycomponent])



def rotMat(phi:"float") -> "list[list[float]]":
    """Returns 2x2 rotation matrix. Must be multiplied from the right.

    Args:
        phi (float): Angle of counter-clockwise rotation.

    Returns:
        list[list[float]]: 2x2 rotation matrix
    """
    return np.array([
        [+np.cos(phi), +np.sin(phi)],
        [-np.sin(phi), +np.cos(phi)]
        ])