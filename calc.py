'''
Methods and functions for husimi calculation.
'''

from typing import Literal

import numpy as np
from scipy.integrate import trapezoid, simpson
integrationFunction = simpson

import matplotlib.pyplot as plt

from .file import printProgressBar

# square root of 2 saved just in case
SQRT2:"float" = 1.4142135623730951


###################################################
# coherent state functions
###################################################

def coherentState(
        q:"float|list[float]",       # s coordinates, for which the coherent state is calculated
        q0:"float",                  # positional center of state
        p0:"float",                  # momentum center of state
        wavenumber_gaussian:"float", # wave number for scaling of standard deviance and factor
        wavenumber_fourier:"float",  # wave number for fourier part
        variance_fac:"float"=SQRT2,     # positional sharpness of coherent state
        stateShape:"float"=1e6       # conjugated part of shape parameter (= 1/R0)
        ) -> "complex|list[complex]":
    """Coherent state centered around (q0,p0).

    Args:
        q (float|list[float]): X-coordinates for which the coherent state is calculated
        q0 (float): Spatial center of coherent state
        p0 (float): Momentum center of coherent state
        wavenumber_gaussian (float): Wavenumber governing the variance in spatial dimension.
        wavenumber_fourier (float): Maximum impulse to be found with Fourier transform.
        variance_fac (float, optional): Factor before variance, higher factor leads to higher variance in spatial dimension. Defaults to 1..
        stateShape (float, optional): Governs the shape of the coherent state, contains length scale information. Defaults to 1e6.

    Returns:
        complex|list[complex]: Calue of coherent state at s.
    """

    # normalization factor 
    # default is variance = sqrt(2)/k1, where k1 is the wave vector INSIDE the cavity
    variance:"float" = (variance_fac / wavenumber_gaussian)**1 # stateshape is now here to cancel out the wavenumber gaussian
    normFactor:"float" = np.power(stateShape / (variance * np.pi), 0.25)
    
    # calculation
    fac_pos_args = - stateShape * np.square(q - q0) / (2. * variance)
    fac_pos = np.exp( fac_pos_args )
    fac_momentum = np.exp(1j * wavenumber_fourier * p0 * (q - q0))
    coherentState_values = normFactor * fac_pos * fac_momentum 
    
    # print('wavenum :\t{0:.6e}'.format(wavenumber))
    # print('normfac :\t{0:.6e}'.format(normFactor))
    # print('\t\t{0:.3e} * {1:.3e}'.format(1. / (2. * variance), np.square(np.mean(s - s0))))
    # print('fac_pos_args:\n{0}\nfac_pos:\n{1}\n{2}'.format(fac_pos_args, fac_pos, np.square(s - s0) / (2. * variance)))
    

    # finished!
    return coherentState_values



def periodicCoherentState(
        s:"float|list[float]",
        s0:"float",
        p0:"float",
        wavenumber_gaussian:"float",
        wavenumber_fourier:"float",
        cavityArclength:"float",
        variance_fac:"float"=SQRT2,
        stateShape:"float"=1e6,
        n_repetitions:"int" = 2
        ) -> "complex|list[complex]":
    """Periodic coherent state centered around (s0,p0).

    Args:
        s (float|list[float]): X-coordinates for which the coherent state is calculated
        s0 (float): Spatial center of coherent state
        p0 (float): Momentum center of coherent state
        wavenumber_gaussian (float): Wavenumber governing the variance in spatial dimension.
        wavenumber_fourier (float): Maximum impulse to be found with Fourier transform.
        cavityArclength (float): Arclength of microcavity
        variance_fac (float, optional): Factor before variance, higher factor leads to higher variance in spatial dimension. Defaults to 1..
        stateShape (float, optional): Governs the shape of the coherent state, contains length scale information. Defaults to 1e6.
        n_repetitions (int, optional): Amount of periodic coherent state shifts to positive and negative spatial coordinates. A value of 0 would reflect a non-periodic state. Defaults to 2.

    Returns:
        complex|list[complex]: Calue of coherent state at s.
    """
    
    # initialize
    coherentState_values = coherentState(s, s0, p0, wavenumber_gaussian, wavenumber_fourier, variance_fac, stateShape)
    
    # periodic boundary conditions
    for mm in np.arange(1, n_repetitions+1):
        # extension to positive sign
        coherentState_values = coherentState_values + coherentState(s, s0 - mm * cavityArclength, p0, wavenumber_gaussian, wavenumber_fourier, variance_fac, stateShape)
        # extension to negative sign
        coherentState_values = coherentState_values + coherentState(s, s0 + mm * cavityArclength, p0, wavenumber_gaussian, wavenumber_fourier, variance_fac, stateShape)
    
    # finished!
    return coherentState_values



###################################################
# husimi calculation
###################################################

husimiPoint_plotCounter:"int" = -1

def calculate_husimiPoint(
        q_now_unitless:"float",
        p_now_unitless:"float",
        field_normal:"list[float]|list[complex]",
        field_derivative:"list[float]|list[complex]",
        arclength_list:"list[float]",
        cavityArclength:"float",
        wavenumber_inCavity_per_m:"float",
        wavenumber_outsideCavity_per_m:"float",
        refractiveIndex_inCavity:"float",
        refractiveIndex_outsideCavity:"float",
        loc:"Literal['inside', 'outside']",               # inside   | outside
        direction:"Literal['incident', 'emerging']",       # incident | emerging
        lengthScale:"float"=1e-6,    # unit length in simulation
        logging:"bool"=False,
        variance_fac:"float" = SQRT2
        ) -> float:
    """Returns the Husimi function at phase-space coordinates (q,p).

    Args:
        q_now_unitless (float): Unitless phase space location coordinate.
        p_now_unitless (float): Unitless phase space momentum coordinate.
        field_normal (list[float]|list[complex]): Field value on boundary of cavity. Use float list for a snapshot if the husimi function at a specific time, use complex values for husimi of complete period.
        field_derivative (list[float]|list[complex]): Derivative value on boundary of cavity. Use float list for a snapshot if the husimi function at a specific time, use complex values for husimi of complete period.
        arcLenth_list (list[float]): Arclengths corresponding to field values in m.
        cavityArclength (float): Total arclength of cavity, used for conversion of q from untiless to m.
        wavenumber_inCavity_per_m (float): Wavenumber inside the cavity in 1/m.
        wavenumber_outsideCavity_per_m (float): Wavenumber outside the cavity in 1/m.
        refractiveIndex_inCavity (float): Refractive index inside the cavity.
        refractiveIndex_outsideCavity (float): Refractive index outside the cavity.
        loc (Literal[&#39;inside&#39;, &#39;outside&#39;]): Calculate husimi function inside or outside the cavity
        direction (Literal[&#39;incident&#39;, &#39;emerging&#39;]): Calculate incident or emerging husimi function.
        lengthScale (float): Length of unit cell in 1/m. Defaults to micrometers.
        logging (bool): Enables logging of value calculation.
        variance_fac (float): Factor to coherent state width. Defaults to sqrt(2)=1.14.

    Raises:
        ValueError: Raised when invalid loc or direction are chosen.

    Returns:
        float: Value of Husimi function at (q,p).
    """


    

    q_now_m:"float" = q_now_unitless * cavityArclength


    if loc == 'inside':
        wavenumber_here:"float" = wavenumber_inCavity_per_m
    elif loc == 'outside':
        wavenumber_here:"float" = wavenumber_outsideCavity_per_m
    else:
        raise ValueError('loc "{0}" not a valid location'.format(loc))

    # calculate coherent state
    coherentState_now:"list[complex]" = periodicCoherentState(
        arclength_list, 
        q_now_m,
        p_now_unitless,
        wavenumber_inCavity_per_m,
        wavenumber_here,
        cavityArclength,
        variance_fac=variance_fac,
        stateShape=1./lengthScale
        )


    # calculate modulations of fields
    modulated_normalField:"list[complex]" = field_normal * coherentState_now           # field on edge is the same for inside and outside
    modulated_derivativeField:"list[complex]" = field_derivative * coherentState_now

    # integration
    integralValue_normalField:"complex" = integrationFunction(
        y=modulated_normalField, 
        x=arclength_list
        )
    integralValue_derivativeField:"complex" = integrationFunction(
        y=modulated_derivativeField, 
        x=arclength_list
        )


    # apply angular momentum weight
    if loc == 'inside':
        angularMomentum_refractiveIndex = refractiveIndex_inCavity
    elif loc == 'outside':
        angularMomentum_refractiveIndex = refractiveIndex_outsideCavity
    else:
        raise ValueError('loc "{0}" is not a valid argument'.format(loc))

    angularMomentumWeight_normal:"float" = np.sqrt( angularMomentum_refractiveIndex * np.sqrt(1.-p_now_unitless**2) )
    angularMomentumWeight_derivative:"float" = 1.j / (wavenumber_outsideCavity_per_m * angularMomentumWeight_normal)


    # husimiparts

    # normal
    if loc == 'inside':
        normalPart_factor = -1.
    elif loc == 'outside':
        normalPart_factor = +1.
    else:
        raise ValueError('loc "{0}" is not a valid argument'.format(loc))

    husimiPart_normalField:"complex" = normalPart_factor * angularMomentumWeight_normal * integralValue_normalField


    # derivative
    if direction == 'incident':
        derivativePart_factor = +1.
    elif direction == 'emerging':
        derivativePart_factor = -1.
    else:
        raise ValueError('direction "{0}" is not a valid argument'.format(direction))

    husimiPart_derivativeField:"complex" = derivativePart_factor * angularMomentumWeight_derivative * integralValue_derivativeField


    # calculating husimi
    husimiFunction_raw:"complex" = husimiPart_normalField + husimiPart_derivativeField
    husimiFunction:"float" = wavenumber_here / np.pi * 2 * np.abs(husimiFunction_raw)**2



    if logging: 
        # print('{0:.3e} + {1:.3e} = {2:.3e} -> {3:.3e}'.format(husimiPart_normalField, husimiPart_derivativeField, husimiFunction_raw, husimiFunction))
        
        style_realPart = 'k-'
        style_imagPart = 'r--'
        # plotting how value came to be
        
        xPlot_unitless = arclength_list / cavityArclength
        
        fig, axes = plt.subplots(
            ncols=3,
            nrows=2, 
            sharex=True,
            # figsize=(15,7.5)
            figsize=(15,5)
            )
        
        ax_field_normal, ax_field_derivative = axes[:,0]
        ax_modulated_normal, ax_modulated_derivative = axes[:,1]
        ax_coherent = axes[0,2]
        
        
        # field functions
        
        ax_field_normal.plot(
            xPlot_unitless, np.real(field_normal),
            style_realPart
            )
        ax_field_normal.plot(
            xPlot_unitless, np.imag(field_normal),
            style_imagPart
            )
        ax_field_normal.set_title('field')
        
        
        ax_field_derivative.plot(
            xPlot_unitless, np.real(field_derivative),
            style_realPart
            )
        ax_field_derivative.plot(
            xPlot_unitless, np.imag(field_derivative),
            style_imagPart
            )
        ax_field_derivative.set_title('field derivative')
        
        
        # modulated functions
        
        ax_modulated_normal.plot(
            xPlot_unitless, np.real(modulated_normalField),
            style_realPart
            )
        ax_modulated_normal.plot(
            xPlot_unitless, np.imag(modulated_normalField),
            style_imagPart
            )
        ax_modulated_normal.text(
            0.5, 0.95, 'integral: {0:.2e}'.format(integralValue_normalField),
            ha='center', va='top',
            transform=ax_modulated_normal.transAxes,
            bbox=dict(fc='white', ec='black')
            )
        ax_modulated_normal.set_title('field modulated')
        
        
        
        ax_modulated_derivative.plot(
            xPlot_unitless, np.real(modulated_derivativeField),
            style_realPart
            )
        ax_modulated_derivative.plot(
            xPlot_unitless, np.imag(modulated_derivativeField),
            style_imagPart
            )
        ax_modulated_derivative.text(
            0.5, 0.95, 'integral: {0:.2e}'.format(integralValue_derivativeField),
            ha='center', va='top',
            transform=ax_modulated_derivative.transAxes,
            bbox=dict(fc='white', ec='black')
            )
        ax_modulated_derivative.set_title('derivative modulated')
        
        
        
        # coherent states

        ax_coherent.plot(
            xPlot_unitless, np.real(coherentState_now),
            style_realPart
            )

        ax_coherent.plot(
            xPlot_unitless, np.imag(coherentState_now),
            style_imagPart
            )
        ax_coherent.set_title('coherent state')
        
        # mark q location
        # ax_modulated_derivative.set_xlim(q_now_unitless-.15,q_now_unitless+.15)
        ax_modulated_derivative.set_xlim(0,1)
        
        for ax in axes.flatten():
            ax.axvline(q_now_unitless, color='b')
            
        for ax in axes[1]:
            ax.set_xlabel(r'arclength $s/L$')

        global husimiPoint_plotCounter
        husimiPoint_plotCounter = husimiPoint_plotCounter + 1
        # print('husimiPoint logging: plotting #{0}'.format(husimiPoint_plotCounter))
        plotname = 'husimiPoint_{2}_q={0:.3f}_p={1:.3f}.png'.format(q_now_unitless, p_now_unitless, husimiPoint_plotCounter)
        fig.savefig(plotname, bbox_inches='tight', dpi=200)
        plt.close(fig)


    # finished
    return husimiFunction
    



def calculate_husimi(
        q_unitless_list:"list[float]",
        p_unitless_list:"list[float]",
        field_normal:"list[float]|list[complex]",
        field_derivative:"list[float]|list[complex]",
        arclength_list:"list[float]",
        cavityArclength:"float",
        wavenumber_inCavity_per_m:"float",
        wavenumber_outsideCavity_per_m:"float",
        refractiveIndex_inCavity:"float",
        refractiveIndex_outsideCavity:"float",
        loc:"Literal['inside', 'outside']",               # inside   | outside
        direction:"Literal['incident', 'emerging']",       # incident | emerging
        lengthScale:"float"=1e-6, # length of unit size
        logging:"bool"=False,
        variance_fac:"float"=SQRT2
        ) -> "list[list[float]]":
    """Returns the Husimi function at phase-space coordinates (q,p).

    Args:
        q_unitless_list (float): List of unitless phase space location coordinates.
        p_unitless_list (float): List of unitless phase space momentum coordinates.
        field_normal (list[float]|list[complex]): Field value on boundary of cavity. Use float list for a snapshot if the husimi function at a specific time, use complex values for husimi of complete period.
        field_derivative (list[float]|list[complex]): Derivative value on boundary of cavity. Use float list for a snapshot if the husimi function at a specific time, use complex values for husimi of complete period.
        arcLenth_list (list[float]): Arclengths corresponding to field values in m.
        cavityArclength (float): Total arclength of cavity, used for conversion of q from untiless to m.
        wavenumber_inCavity_per_m (float): Wavenumber inside the cavity in 1/m.
        wavenumber_outsideCavity_per_m (float): Wavenumber outside the cavity in 1/m.
        refractiveIndex_inCavity (float): Refractive index inside the cavity.
        refractiveIndex_outsideCavity (float): Refractive index outside the cavity.
        loc (Literal[&#39;inside&#39;, &#39;outside&#39;]): Calculate husimi function inside or outside the cavity
        direction (Literal[&#39;incident&#39;, &#39;emerging&#39;]): Calculate incident or emerging husimi function.
        lengthScale (float): Length of unit cell in 1/m.
        logging (bool): Enables plotting the steps to calculate each value of H(q,p)
        variance_fac (float): Factor to coherent state width. Defaults to sqrt(2)=1.14.

    Raises:
        ValueError: Raised when invalid loc or direction are chosen.

    Returns:
        float: Value of Husimi functions for all (q,p).
    """


    # initialize
    husimiFunction = np.zeros(
        (len(p_unitless_list), len(q_unitless_list)),
        dtype=float
        )
    
    # initialize progress bar
    progressBar_params = dict(
        prefix = 'Progress:',
        suffix = 'Complete',
        length = 50
        )
    
    totalIterations:"int" = len(q_unitless_list) * len(p_unitless_list)
    iteration:"int" = 1
    printProgressBar(0, totalIterations, **progressBar_params)
    
    for ii, q_now in enumerate(q_unitless_list):
        for jj, p_now in enumerate(p_unitless_list):
            printProgressBar(iteration, totalIterations, **progressBar_params)
            
            husimiFunction_now = calculate_husimiPoint(
                q_now, p_now,
                field_normal,
                field_derivative,
                arclength_list,
                cavityArclength,
                wavenumber_inCavity_per_m,
                wavenumber_outsideCavity_per_m,
                refractiveIndex_inCavity,
                refractiveIndex_outsideCavity,
                loc,
                direction,
                lengthScale,
                logging=logging,
                variance_fac=variance_fac
                )
            # print(husimiFunction_now)
            husimiFunction[jj,ii] = husimiFunction_now
            
            iteration += 1


    # finished
    return husimiFunction




def calculate_husimi_chi(
        q_unitless_list:"list[float]",
        chi_unitless_list:"list[float]",
        field_normal:"list[float]|list[complex]",
        field_derivative:"list[float]|list[complex]",
        arclength_list:"list[float]",
        cavityArclength:"float",
        wavenumber_inCavity_per_m:"float",
        wavenumber_outsideCavity_per_m:"float",
        refractiveIndex_inCavity:"float",
        refractiveIndex_outsideCavity:"float",
        loc:"Literal['inside', 'outside']",               # inside   | outside
        direction:"Literal['incident', 'emerging']",       # incident | emerging
        lengthScale:"float"=1e-6 # length of unit size
        ) -> "list[list[float]]":
    """Returns the Husimi function at phase-space coordinates (q,chi). For translation from Husimi to farfield the impulses to be calculated are different,
    as the outgoing angle is of interest. p = sin(chi)

    Args:
        q_unitless_list (float): List of unitless phase space location coordinates.
        p_unitless_list (float): List of unitless phase space momentum coordinates as angle.
        field_normal (list[float]|list[complex]): Field value on boundary of cavity. Use float list for a snapshot if the husimi function at a specific time, use complex values for husimi of complete period.
        field_derivative (list[float]|list[complex]): Derivative value on boundary of cavity. Use float list for a snapshot if the husimi function at a specific time, use complex values for husimi of complete period.
        arcLenth_list (list[float]): Arclengths corresponding to field values in m.
        cavityArclength (float): Total arclength of cavity, used for conversion of q from untiless to m.
        wavenumber_inCavity_per_m (float): Wavenumber inside the cavity in 1/m.
        wavenumber_outsideCavity_per_m (float): Wavenumber outside the cavity in 1/m.
        refractiveIndex_inCavity (float): Refractive index inside the cavity.
        refractiveIndex_outsideCavity (float): Refractive index outside the cavity.
        loc (Literal[&#39;inside&#39;, &#39;outside&#39;]): Calculate husimi function inside or outside the cavity
        direction (Literal[&#39;incident&#39;, &#39;emerging&#39;]): Calculate incident or emerging husimi function.
        lengthScale (float): Length of unit cell in 1/m.

    Raises:
        ValueError: Raised when invalid loc or direction are chosen.

    Returns:
        float: Value of Husimi functions for all (q,chi).
    """

    p_unitless_list = np.sin(chi_unitless_list * .5 * np.pi)

    # initialize
    husimiFunction = np.zeros(
        (len(chi_unitless_list), len(q_unitless_list)),
        dtype=float
        )
    
    # initialize progress bar
    progressBar_params = dict(
        prefix = 'Progress:',
        suffix = 'Complete',
        length = 50
        )
    
    totalIterations:"int" = len(q_unitless_list) * len(p_unitless_list)
    iteration:"int" = 1
    printProgressBar(0, totalIterations, **progressBar_params)
    
    for ii, q_now in enumerate(q_unitless_list):
        for jj, p_now in enumerate(p_unitless_list):
            printProgressBar(iteration, totalIterations, **progressBar_params)
            
            husimiFunction_now = calculate_husimiPoint(
                q_now, p_now,
                field_normal,
                field_derivative,
                arclength_list,
                cavityArclength,
                wavenumber_inCavity_per_m,
                wavenumber_outsideCavity_per_m,
                refractiveIndex_inCavity,
                refractiveIndex_outsideCavity,
                loc,
                direction,
                lengthScale
                )
            # print(husimiFunction_now)
            husimiFunction[jj,ii] = husimiFunction_now
            
            iteration += 1


    # finished
    return husimiFunction