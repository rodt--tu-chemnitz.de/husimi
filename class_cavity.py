"""Useful functions for limacon treatment
"""

import numpy as np
from scipy.integrate import quad
from typing import Tuple

class Cavity():
    def __init__(
            self,
            R0:"float"=1.,
            xpos:"float" = 0.,
            ypos:"float" = -0.4,
            ) -> None:
        
        self.R0:"float" = R0         # radius of corresponding disc
        self.deform:"float" = 0.
        
        self.xpos:"float" = xpos     # coordinate origin in x
        self.ypos:"float" = ypos     # coordinate origin in y
        
        self.phi0:"float" = 0.     # rotation angle to be overridden, makes no sense for discs
        
        return None
    

    ###########################################
    # base definitions
    ###########################################

    def radius(self, phi=0.) -> "float":
        return self.R0


    def radiusDerivative(self, phi=0.) -> "float":
        return 0.

    
    
    ###########################################
    # boundary coordinates
    ###########################################


    def x(self, phi:"float") -> "float":
        return - np.sin(phi+self.phi0) * self.radius(phi) + self.xpos


    def y(self, phi:"float") -> "float":
        return np.cos(phi+self.phi0) * self.radius(phi) + self.ypos


    def r(self, phi:"float") -> "Tuple[float, float]":
        return np.array([self.x(phi), self.y(phi)])
    
    
    
    ###########################################
    # boundary properties
    ###########################################


    def lengthElement(self, phi:"float") -> "float":
        return np.sqrt( self.radius(phi)**2 + self.radiusDerivative(phi)**2 )


    def arclength(self, phi:"float") -> "float":
        integral = quad(
            lambda x: self.lengthElement(x),
            0,
            phi,
            complex_func=False
            )
        return integral[0] # only return value, not the error
    
    
    def arclength_list(self, phi_list:"list[float]") -> "list[float]":
        return np.array( [self.arclength(phi) for phi in phi_list] )


    def normalVector(self, phi:"float") -> "Tuple[float, float]":
        """points outside
        """
        rr = self.radius(phi+self.phi0)
        rrDer = self.radiusDerivative(phi+self.phi0)
        
        sinphi = np.sin(phi+self.phi0)
        cosphi = np.cos(phi+self.phi0)
        
        component_x = (+ rrDer * cosphi - rr * sinphi)
        component_y = (+ rrDer * sinphi + rr * cosphi)
        
        normalVector = np.array([component_x, component_y])
        
        # normalization
        vectorNorm = self.lengthElement(phi+self.phi0)
        normalVector_normed = normalVector / vectorNorm


        return normalVector_normed
        
        

    def normalVector_outwards(self, phi:"float") -> "Tuple[float, float]":
        """points outside
        """
        return + self.normalVector(phi)

    def normalVector_inwards(self, phi:"float") -> "Tuple[float, float]":
        """points inside
        """
        return - self.normalVector(phi)
    
    
    
    ###########################################
    # boundary coordinate methods
    ###########################################
    
    def get_boundary(self, n_points:"int"=101) -> "Tuple[list[float], list[float], list[float]]":
        phi_list:"list[float]" = np.linspace(0, 1, n_points) * 2. * np.pi
        boundary:"Tuple[list[float], list[float]]" = self.r(phi_list)
        return np.array([phi_list, *boundary])