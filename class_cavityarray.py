"""Class contains cavities
"""

from typing import Any, Tuple
import numpy as np
import matplotlib.pyplot as plt

# for boundaryfunction determination
from scipy.interpolate import LinearNDInterpolator
import numdifftools as nd

from .class_cavity_limacon import Limacon
from .class_mode import Mode
from .calc import calculate_husimi


class CavityArray():
    def __init__(self) -> None:
        # cavities
        self.cavities:"list[Limacon]" = []
        
        # field from wave calculations
        self.mode:"Mode" = None
        
        return None
    
    ###########################################
    # properties
    ###########################################
    
    @property
    def n_cavities(self) -> "int":
        return len(self.cavities)
    
    
    @property
    def field_values_normed(self) -> "list[complex]":
        return self.mode.zcoords / np.max(np.abs(self.mode.zcoords))

    
    ###########################################
    # array manipulation
    ###########################################
    
    def add_limacon(self, **lima_kwargs) -> None:
        lima = Limacon(**lima_kwargs)
        self.cavities.append( lima )
        return None
    
    
    def make_lima_array(self, 
            n_cavities:"int", 
            centerdist:"float", 
            rotationAngles:"None|list[float]"=None,    # in rad
            deforms:"None|list[float]"=None,
            **lima_kwargs
            ) -> None:
        # array is centered around (0,0) and extends in x
        # starts to the left 
        array_x0:"float" = - centerdist * (n_cavities-1) * .5
        
        # if no rotationanles are set, they default to 0 degrees
        if rotationAngles is None:
            rotationAngles:"list[float]" = np.zeros(n_cavities)
        
        if deforms is None:
            deforms:"list[float]" = np.ones(n_cavities) * .4
        
        
        
        for cavityIndex, (phi0, deform) in enumerate(zip(rotationAngles, deforms)):
            # calculate shift due to phi0
            # shift is in the opposite direction than phi=0
            vector_to_front = np.array([
                - np.sin(phi0), np.cos(phi0)
                ]) * deform
            shift_xpos = - vector_to_front[0]
            shift_ypos = - vector_to_front[1]
            
            xpos:"float" = array_x0 + cavityIndex * centerdist + shift_xpos
            ypos:"float" = shift_ypos
            
            self.add_limacon(
                xpos=xpos, ypos=ypos, 
                phi0=phi0, 
                deform=deform, 
                **lima_kwargs
                ) # throws error if xpos is in lima_kwargs
        
        return None

    
    
    ###########################################
    # array information
    ###########################################
    
    def get_cavity_boundaries(self, n_points=101) -> "list[Tuple[list[float], list[float], list[float]]]":
        boundarycoords_list:"list[Tuple[list[float], list[float], list[float]]]" = []
        
        for cav in self.cavities:
            boundarycoords_now:"Tuple[list[float], list[float], list[float]]" = cav.get_boundary(n_points)
            boundarycoords_list.append( boundarycoords_now )
        
        return np.array(boundarycoords_list)
    
    
    
    def plot_field(
            self, 
            plotName:"str" = 'array.png', 
            func:"str"=None, 
            vmin=None, 
            vmax=None,
            n_levels=100,
            cmap='nipy_spectral',
            ) -> None:

        if func is None:
            plotData = np.abs(self.mode.zcoords)**2
            colorbarlabel = r'$|E_z|^2$ [a.u.]'
            
            if vmin is None: vmin = 0
            if vmax is None: vmax = 1
            levels = np.linspace(vmin,vmax,n_levels+1)
            colorbarticks = [0, 0.2, 0.4, 0.6, 0.8, 1.0]
            
        elif func == 'sqrt':
            plotData = np.sqrt( np.abs( self.mode.zcoords ) )
            colorbarlabel = r'$\sqrt{|E_z|}$ [a.u.]'
            
            if vmin is None: vmin = 0
            if vmax is None: vmax = 1
            levels = np.linspace(vmin,vmax,n_levels+1)
            colorbarticks = [0, 0.2, 0.4, 0.6, 0.8, 1.0]
            
        elif func == 'log':
            plotData = self.mode.zcoords_intensity_log
            colorbarlabel = r'$\log_{10} |E_z|^2$'
            
            if vmin is None: vmin = -5
            if vmax is None: vmax = 0
            levels = np.linspace(vmin,vmax,n_levels+1)
            colorbarticks = np.arange(np.floor(vmin), np.ceil(vmax)+1e-3, 1)
            
        else:
            print('Error: func {0} not recognized'.format(func))
            raise ValueError

        cmap = plt.get_cmap(cmap)
        cmap.set_over('w')
        cmap.set_under('k')
        
        
        fig, ax = plt.subplots()

        im = ax.tricontourf(
            self.mode.xcoords, self.mode.ycoords, 
            plotData,
            cmap=cmap,
            extend='both',
            levels=levels,
            vmin=vmin, vmax=vmax
            )
        
        fig.colorbar(
            im,
            shrink=0.3,
            ticks=colorbarticks,
            label=colorbarlabel
            )
        
        for _, xx, yy in self.get_cavity_boundaries():
            ax.plot(xx, yy, 'k-', lw='0.5')
        
        
        # boundaries
        dpml_um:"float" = 1. # thicknes of PML
        extracut:"float" = 0. # cut more from the side for more zoom on the cavities
        totalcut:"float" = dpml_um + extracut
        
        min_x = self.mode.min_x + totalcut
        max_x = self.mode.max_x - totalcut
        ax.set_xlim(min_x, max_x)
        
        min_y = self.mode.min_y + totalcut
        max_y = self.mode.max_y - totalcut
        ax.set_ylim(min_y, max_y)
        
        
        ax.set_aspect('equal')

        fig.tight_layout()
        fig.savefig(plotName)
        plt.close(fig)
        
        return None
    
        
    
    
    ###########################################
    # array field
    ###########################################

    
    
    def set_mode(self, mode:"Mode") -> None:
        self.mode:"Mode" = mode
        return None
    
    
    def set_field(self, 
            xx:"list[float]", 
            yy:"list[float]", 
            zz:"list[complex]",
            calcNorm:"bool"=False,
            scaleMode:"bool"=True,
            **kwargs
            ) -> None:
        mode = Mode(xx, yy, zz, calcNorm=calcNorm, scaleMode=scaleMode, **kwargs)
        self.set_mode(mode)
        return None
    
    
    def field(self, xx:"float", yy:"float") -> complex:
        return self.mode.field(xx, yy)
    
    
    ###########################################
    # husimi calculation
    ###########################################
    
    
    def get_boundaryfunc_oneCavity(self, cavity:"Limacon", n_points=1001) -> "Tuple[list[float], list[complex], list[complex]]":
        arclength_list:"list[float]" = []
        func_normal_list:"list[complex]" = []
        func_derivative_list:"list[complex]" = []
        
        # iterating over boundary points
        for phi, xx, yy in zip(*cavity.get_boundary(n_points)):
            arclength = cavity.arclength(phi)
            arclength_list.append(arclength)
            
            func_normal:"complex" = self.mode.field(xx, yy)
            func_normal_list.append(func_normal)
            
            func_derivative:"complex" = self.mode.gradient(xx, yy) @ cavity.normalVector_outwards(phi)
            func_derivative_list.append(func_derivative)
        
        
        arclength_list = np.array(arclength_list)
        func_normal_list = np.array(func_normal_list)
        func_derivative_list = np.array(func_derivative_list)
        
        return [arclength_list, func_normal_list, func_derivative_list]
    
    
    
    def get_boundaryfunc(self, n_points=1001) -> "Tuple[list[float], list[complex], list[complex]]":
        # lists for 1 cavity
        arclength_list:"list[float]"
        func_normal_list:"list[complex]"
        func_derivative_list:"list[complex]"
        
        # lists for all cavities
        arclength_all_list:"list[list[float]]" = []
        func_normal_all_list:"list[list[complex]]" = []
        func_derivative_all_list:"list[list[complex]]" = []

        
        for cavity in self.cavities:
            arclength_list, func_normal_list, func_derivative_list = self.get_boundaryfunc_oneCavity(cavity, n_points)
            
            arclength_all_list.append(arclength_list)
            func_normal_all_list.append(func_normal_list)
            func_derivative_all_list.append(func_derivative_list)
        
        
        
        arclength_all_list = np.array(arclength_all_list)
        func_normal_all_list = np.array(func_normal_all_list)
        func_derivative_all_list = np.array(func_derivative_all_list)
        
        
        
        return [arclength_all_list, func_normal_all_list, func_derivative_all_list]

    
    def calculate_husimi_singleCavity(self, 
            cavity:"Limacon", 
            wavenumber_inCavity_per_um:"float", wavenumber_outsideCavity_per_um:"float",
            refractiveIndex_inside:"float",     refractiveIndex_outside:"float",
            loc:"str"='inside',                 direction:str='incident',
            # lengthScale=1e-6, disabled, working im um
            n_qpoints:"int" = 201, n_ppoints:"int" = 101,
            n_boundaryfuncpoints:"int"=1001
            ) -> "Tuple[list[float], list[float], list[list[float]]]":
        
        
        # obtain boundary function
        arclength_list, func_normal_list, func_derivative_list = self.get_boundaryfunc_oneCavity(cavity, n_boundaryfuncpoints)
        cavityArclength = arclength_list[-1]
        
        q_list_unitless = np.linspace(0,1,n_qpoints)
        p_list_unitless = np.linspace(-1,1,n_ppoints+2)[1:-1]
        
        husimi_inner_inc = calculate_husimi(
            q_list_unitless,             p_list_unitless,
            func_normal_list,            func_derivative_list,
            arclength_list,              cavityArclength,
            wavenumber_inCavity_per_um,  wavenumber_outsideCavity_per_um,
            refractiveIndex_inside,      refractiveIndex_outside,
            loc,                         direction,
            lengthScale=1e0
            )
        
        
        return [q_list_unitless, p_list_unitless, husimi_inner_inc]
