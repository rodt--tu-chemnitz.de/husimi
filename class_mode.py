"""This class is for modes for orthogonalization purposes
"""

from typing import Tuple

import numpy as np
from scipy.interpolate import LinearNDInterpolator
import numdifftools as nd

from .file import printProgressBar

class Mode():
    #####################################################
    # Initializing
    #####################################################
    
    def __init__(self, 
            xcoords:"list[float]",
            ycoords:"list[float]", 
            zcoords:"list[complex]", 
            integrationDelta:"float"=0.01,
            gradientDelta:"float"=0.01,
            calcNorm:"bool"=True,           # if mode is already normed, the norm is set to 1
            scaleMode:"bool"=True,          # scale saved zcoords so that max intensity is 1
            ) -> None:
        
        # store input values
        self.xcoords:"list[float]" = xcoords
        self.ycoords:"list[float]" = ycoords
    
        self.zcoords:"list[complex]" = zcoords
        if scaleMode: self.zcoords = self.zcoords / np.max(np.abs(zcoords))
        
        # create interpolationfunctions
        self._zcoordsInterpolationFunc:"function" = self.create_zcoordsInterpolation()
        self._intensityInterpolationFunc:"function" = self.create_intensityInterpolation()
        self._gradientFunction:"function" = self.create_gradientInterpolation(step=gradientDelta)
        
        # create parameters
        if calcNorm:
            self.norm:"float" = self.calculate_norm(integrationDelta)
        else:
            self.norm:"float" = 0.
        
        return None

    
    
    def create_zcoordsInterpolation(self) -> LinearNDInterpolator:
        """returns interpolated field function"""
        # set function    
        return LinearNDInterpolator(
            # datapoints
            np.transpose((self.xcoords, self.ycoords)),
            # field
            self.zcoords,
            fill_value=0.
            )
    
    
    
    def create_intensityInterpolation(self) -> LinearNDInterpolator:
        """returns interpolated field function"""
        # set function    
        return LinearNDInterpolator(
            # datapoints
            np.transpose((self.xcoords, self.ycoords)),
            # field
            self.zcoords_intensity,
            fill_value=0.
            )
    
    
    
    def create_gradientInterpolation(self, step:"float"=0.05) -> nd.Gradient:
        return nd.Gradient(
            lambda xy: self._zcoordsInterpolationFunc(xy[0], xy[1]),
            step=step # empirical value, for this step size the gradient looks "smooth" with converged value
            )
    
    
    
    
    #####################################################
    # Properties
    #####################################################
    
    
    @property
    def zcoords_conj(self) -> "list[complex]":
        """conjugated field"""
        return np.conj(self.zcoords)
    
    @property
    def zcoords_intensity(self) -> "list[float]":
        """field intensity"""
        return np.real( self.zcoords_conj * self.zcoords ) # make real, as field is real

    @property
    def zcoords_intensity_log(self) -> "list[float]":
        """10er logarithm of field intensity.
        
        The intensity values are so low at certain coordinates, that log gives -inf, those values are set to 0.
        """
        logz:"list[float]" = np.log10(self.zcoords_intensity)
        logz[logz == -np.inf] = 0.
        return logz


    # cell limits
    @property
    def min_x(self) -> "float": return np.min(self.xcoords)
    @property
    def max_x(self) -> "float": return np.max(self.xcoords)
    @property
    def min_y(self) -> "float": return np.min(self.ycoords)
    @property
    def max_y(self) -> "float": return np.max(self.ycoords)

    @property
    def boundingBox(self) -> "list[float]":
        bbox:"list[float]" = np.array([ self.min_x, self.max_x, self.min_y, self.max_y ])
        return bbox
    
    
    @property
    def zcoords_normed(self) -> "list[complex]":
        """norm of these field coords is 1"""
        return self.zcoords / np.sqrt(self.norm)
    
    
    #####################################################
    # field functions
    #####################################################
    
    def field(self, x:"float", y:"float") -> "complex":
        """Interpolated field function.

        Args:
            x (float): Coordinate.
            y (float): coordinate.

        Returns:
            complex: Ez value
        """
        return self._zcoordsInterpolationFunc(x, y)
    
    
    
    def gradient(self, x:"float", y:"float") -> "Tuple[complex, complex]":
        """Interpolated gradient function.

        Args:
            x (float): Coordinate.
            y (float): Coordinate.

        Returns:
            Tuple[complex, complex]: Vector of complex gradient values.
        """
        return self._gradientFunction([x,y])



    #####################################################
    # field properties
    #####################################################

    def overlapFunc(self,
            comparison_mode:"Mode",
            xx:"float", 
            yy:"float"
            ) -> "complex":
        """Returns the value of the overlap of the two modes at (xx,yy)."""
        return np.conj(comparison_mode._zcoordsInterpolationFunc(xx,yy)) * self._zcoordsInterpolationFunc(xx, yy)



    def get_overlapFunc(self,
            comparison_mode:"Mode"
            ) -> "function":
        """Returns the overlap function of the two modes as a callable."""
        func:"function" = lambda xx, yy: self.overlapFunc(comparison_mode, xx, yy)
        return func



    def calculate_overlap(self,      
            comparison_mode:"Mode",
            integrationDelta:"float" = 0.01,
            normresult:"bool" = True
            ) -> "complex":
        """Calculate the overlap / inner product with another mode WITHOUT norming it.

        Args:
            comparison_mode (Mode): mode to compare with
            integrationDelta (float, optional): Distance between raster points. Defaults to 0.01.
            normresult (bool, optional): Norm the result by deviding the integral by square root of norms.

        Returns:
            complex: Overlap
        """

        
        # get coordinate bounds
        min_x, max_x, min_y, max_y = self.boundingBox
        
        # start values
        x0:"float" = min_x + integrationDelta*.5
        y0:"float" = min_y + integrationDelta*.5

        
        
        # loop
        
        overlapFunc:"function" = self.get_overlapFunc(comparison_mode)
        integrationValue:"complex" = 0.+0j
        
        x_list:"list[float]" = np.arange(x0, max_x, integrationDelta)
        y_list:"list[float]" = np.arange(y0, max_y, integrationDelta)
        
        
        n_x:"int" = x_list.shape[0]
        n_y:"int" = y_list.shape[0]
        totalIterations:"int" = n_x * n_y
        progressbarParams=dict(
            total=totalIterations,
            prefix='Integration', 
            suffix='Complete'
            )
        
        printProgressBar(0, **progressbarParams)

        for xIndex, xx in enumerate(x_list):
            for yy in y_list:
                # value:"complex" = np.conj(comparison_mode._zcoordsInterpolationFunc(xx,yy)) * self._zcoordsInterpolationFunc(xx, yy)
                value:"complex" = overlapFunc(xx, yy)
                integrationValue = integrationValue + value
                
            printProgressBar((xIndex+1)*n_y, **progressbarParams)
        
        # finalize
        areaPerPoint:"float" = np.square( integrationDelta )
        integrationValue = integrationValue * areaPerPoint


        if normresult:
            integrationValue = integrationValue / ( np.sqrt(self.norm) * np.sqrt(comparison_mode.norm) )


        # end
        return integrationValue
    
    
    
    def calculate_norm(self, integrationDelta:"float" = 0.01):
        norm_complex:"complex" = self.calculate_overlap(
            comparison_mode=self, 
            integrationDelta=integrationDelta,
            normresult=False # don't norm the result, as mode does not necessarily have a norm value. if 
            )
        norm_real:"float" = np.real(norm_complex)
        return norm_real
    

