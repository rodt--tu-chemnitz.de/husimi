'''
Functions for husimi plotting.
'''

import numpy as np
import matplotlib.pyplot as plt

from typing import Tuple


def plot_husimi(
        q_list:"list[float]", 
        p_list:"list[float]",
        husimiData:"list[list[float]]",
        fname:"str"='husimi.png',
        critical_angle:"None|float"=None,
        cmap:"str"='nipy_spectral',
        func=None,
        vmin=None,
        vmax=None,
        ax_use:"None" = None,
        dpi=300
        ) -> "None":
    '''
    plots the husimi data to an existing ax, otherwise creates a new figure
    '''
    
    if func is None:
        plotData = husimiData
        colorbarlabel = r'$H(s, \sin \chi)$ [a.u.]'
        
        if vmin is None: vmin = 0

        
    elif func == 'sqrt':
        plotData = np.sqrt( husimiData )
        colorbarlabel = r'$\sqrt{H(s, \sin \chi)}$ [a.u.]'
        
        if vmin is None: vmin = 0
        
    elif func == 'log':
        plotData = np.log10(husimiData)
        colorbarlabel = r'$\log_{10} H(s, \sin \chi)$'
        
        if vmin is None: vmin = np.max(plotData) - 5 # go over 5 orders of magnitude
        
    else:
        print('Error: func {0} not recognized'.format(func))
        raise ValueError

    
    cmap = plt.get_cmap(cmap)
    cmap.set_over('w')
    cmap.set_under('k')
    
    extent = get_extent(q_list, p_list)



    # plotting
    if ax_use is None:
        fig, ax = plt.subplots(
            figsize=(6,3)
            )
    else:
        ax = ax_use
    
    im = ax.imshow(
        # q_list, p_list,
        plotData,
        extent=extent,
        aspect='auto',
        origin='lower',
        cmap=cmap,
        vmin=vmin,
        vmax=vmax,
        )
    
    if ax_use is None:
        fig.colorbar(im, label=colorbarlabel)
    

    if critical_angle is not None:
        for x in [-1, +1]:
            ax.axhline(
                x * critical_angle,
                color='white',
                linestyle='--',
                linewidth=1.
                )
    
    
    ax.set_xlim(0,1)
    ax.set_ylim(-1,1)

    ax.set_xlabel(r'$s / s_\mathrm{max}$')
    ax.set_ylabel(r'$\sin \chi$')

    
    if ax_use is None:
        # fig.tight_layout()
        fig.savefig(fname,bbox_inches='tight',dpi=dpi)
        plt.close(fig)

    return None




def get_extent(xx:"list[float]", yy:"list[float]") -> "Tuple[float, float, float, float]":
    """Returns the extent for imshow plots, so that the pixel size accurately reflects

    Args:
        xx (list[float]): List of coords in first dimension 
        yy (list[float]): List of coords in second dimension 

    Returns:
        Tuple[float, float, float, float]: Extent.
    """
    
    # first dimension
    xx_min = np.min(xx)
    xx_max = np.max(xx)
    xx_pixelwidth:"float" = np.mean(xx[1:] - xx[:-1])
    xx_minextent = xx_min - xx_pixelwidth * .5
    xx_maxextent = xx_max + xx_pixelwidth * .5
    
    # second dimension
    yy_min = np.min(yy)
    yy_max = np.max(yy)
    yy_pixelwidth:"float" = np.mean(yy[1:] - yy[:-1])
    yy_minextent = yy_min - yy_pixelwidth * .5
    yy_maxextent = yy_max + yy_pixelwidth * .5
    
    extent = [
        xx_minextent, xx_maxextent,
        yy_minextent, yy_maxextent
            ]
    
    return extent