'''
This module contains utilities for extracting farfield from the husimi functions
'''

from typing import Tuple

# import husimi module
import sys
sys.path.append('../..')
import husimi

import numpy as np
import matplotlib.pyplot as plt




def main():
    print('Extracting farfield from husimi')
    
    print('- reading data')
    
    print('\t-> husimi data')
    q_list, p_list, husimiData_outer_em = husimi.file.read_husimi_csv('husimi_outer_em.csv')

    print('\t-> boundary data')
    phi_list, arcLength_list, boundaryX, boundaryY, _, _, = husimi.file.read_comsol_file('boundaryfunc.txt')
    arcLength_max = arcLength_list[-1]
    arcLength_list_unitless = arcLength_list / arcLength_max
    

    print('\t-> wave simulation data')

    comsol_angles, comsol_farfield = np.loadtxt('farfield.txt', skiprows=8).T
    comsol_farfield = comsol_farfield / np.max(comsol_farfield)
    

    print('- calculating farfield from husimi data')

    farfield_angles, farfield_intensities = husimi.farfield.farfield_from_husimi(
        q_list,        p_list,
        husimiData_outer_em,
        arcLength_list_unitless,
        phi_list,
        n_interpolationPoints_s = int(1e2) + 1,
        n_interpolationPoints_chi = int(1e2) + 1,
        n_farfield_angles = int(1e2),
        # intensityFunc=lambda x: x,
        # intensityFunc_inverse=lambda x: x,
        plotting=True,
        boundaryX=boundaryX*1e6,
        boundaryY=boundaryY*1e6
        )


    print('- plotting')

    fig, ax = plt.subplots(subplot_kw=dict(projection='polar'))

    ax.plot(comsol_angles, comsol_farfield, 'b--', label='wave simulation')
    ax.plot(farfield_angles, farfield_intensities, 'ko-', label='husimi extraction')

    ax.legend(framealpha=1.)

    fig.savefig('husimi_farfield.png',dpi=300, bbox_inches='tight')




if __name__ == '__main__':
    main()