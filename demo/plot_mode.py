"""Plots the mode"""

import sys
sys.path.append('../..')
import husimi

import numpy as np
import matplotlib.pyplot as plt


def main() -> None:
    print('Plotting Mode')
    
    
    print('- reading mode data')
    
    x_coords, y_coords, field = husimi.file.read_comsol_file('field.txt', skip_header=9)
    fieldIntensity_abs = np.abs(field)
    fieldIntensity_abs_normed = fieldIntensity_abs / np.max(fieldIntensity_abs)
    
    
    print('- calculating limacon edges')
    
    limaBoundary_deform = 0.4
    limaBoundary_nPoints = 101
    limaBoundary_phi = np.linspace(0,1,limaBoundary_nPoints) * 2. * np.pi
    limaBoundary_radius = 1. + limaBoundary_deform * np.cos(limaBoundary_phi)
    limaBoundary_x = - np.sin(limaBoundary_phi) * limaBoundary_radius
    limaBoundary_y = + np.cos(limaBoundary_phi) * limaBoundary_radius - limaBoundary_deform
    
    
    
    print('- plotting mode')
    
    n_levels = 20
    absPlot_levels = np.linspace(0,1,n_levels)
    absPlot_tickPos = [0,0.25,0.50,0.75,1.00]
    absPlot_cmap = 'viridis'
    
    
    fig, ax = plt.subplots()
    
    im = ax.tricontourf(
        x_coords, y_coords, np.square(fieldIntensity_abs_normed),
        levels=absPlot_levels,
        vmin=0, vmax=1,
        cmap=absPlot_cmap
        )
    
    ax.plot(
        limaBoundary_x, limaBoundary_y,
        'w:',
        linewidth = 1.
        )
    
    fig.colorbar(
        im, 
        label=r'$|E_z|^2$',
        ticks=absPlot_tickPos
        )
    
    ax.set_aspect('equal')
    ax.set_axis_off()
    
    ax.set_xlim(-2.,2.)
    ax.set_ylim(-2.,2.)
    
    fig.tight_layout()
    figName = 'field_normal.png'
    fig.savefig(figName, dpi=300, bbox_inches='tight')
    plt.close()

    
    
    print('- plotting root of mode')
    
    fig, ax = plt.subplots()
    
    im = ax.tricontourf(
        x_coords, y_coords, np.sqrt(fieldIntensity_abs_normed),
        levels=absPlot_levels,
        vmin=0, vmax=1,
        cmap=absPlot_cmap
        )
    
    ax.plot(
        limaBoundary_x, limaBoundary_y,
        'w:',
        linewidth = 1.
        )
    
    fig.colorbar(
        im, 
        label=r'$\sqrt{|E_z|}$',
        ticks=absPlot_tickPos
        )
    
    ax.set_aspect('equal')
    ax.set_axis_off()
    
    ax.set_xlim(-2.,2.)
    ax.set_ylim(-2.,2.)
    
    fig.tight_layout()
    figName = 'field_root.png'
    fig.savefig(figName, dpi=300, bbox_inches='tight')
    plt.close()
    
    
    

    print('- real and complex parts')
    
    realcomplexPlot_levels = np.linspace(-1,1,n_levels)
    realcomplexPlot_tickPos = [-1.0,-0.5,0.0,0.5,1.0]
    realcomplexPlot_field_norm = np.max(np.abs((np.real(field), np.imag(field))))
    realcomplexPlot_field = field / realcomplexPlot_field_norm
    
    fig, (ax_real, ax_imag) = plt.subplots(
        sharex=True, sharey=True,
        ncols=2
        )
    
    
    ax_real.tricontourf(
        x_coords, y_coords, np.real(realcomplexPlot_field),
        levels=realcomplexPlot_levels,
        vmin=-1, vmax=1,
        cmap='coolwarm'
        )
    ax_real.plot(
        limaBoundary_x, limaBoundary_y,
        'k:',
        linewidth = 1.
        )
    ax_real.set_aspect('equal')
    ax_real.set_axis_off()
    ax_real.set_title(r'$\mathrm{Re} \, E_z$')

    im = ax_imag.tricontourf(
        x_coords, y_coords, np.imag(realcomplexPlot_field),
        levels=realcomplexPlot_levels,
        vmin=-1, vmax=1,
        cmap='coolwarm'
        )
    ax_imag.plot(
        limaBoundary_x, limaBoundary_y,
        'k:',
        linewidth = 1.
        )
    ax_imag.set_aspect('equal')
    ax_imag.set_axis_off()
    ax_imag.set_title(r'$\mathrm{Im} \, E_z$')
    
    # fig.colorbar(
    #     im,
    #     ax=ax_imag,
    #     ticks=realcomplexPlot_tickPos
    #     )
    
    ax_real.set_xlim(-2.,2.)
    ax_real.set_ylim(-2.,2.)
    
    fig.tight_layout()
    figName = 'field_RealComplexParts.png'
    fig.savefig(figName, dpi=300, bbox_inches='tight')
    plt.close()
    

    return None


if __name__ == '__main__':
    main()