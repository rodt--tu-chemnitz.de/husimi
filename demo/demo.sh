#!/bin/bash
set -e


# Execute this file to perform the demo


bash make_clean.sh

python plot_mode.py
python calculate_husimi.py
python plot_husimi.py

python extract_farfield.py