'''
plots the calculated husimi functions
'''

import sys
sys.path.append('../..')
import husimi

import numpy as np

def main() -> None:
    print('plotting inner husimi')

    q_list, p_list, husimiData = husimi.file.read_husimi_csv('husimi_inner_inc.csv')

    husimi.plot.plot_husimi(
        q_list, p_list, husimiData,
        fname='husimi_inner_inc.png',
        critical_angle=1./3.
        )
    
    husimi.plot.plot_husimi(
        q_list, p_list, np.power(husimiData, 0.25),
        fname='husimi_inner_inc_root.png',
        critical_angle=1./3.
        )
    

    
    print('plotting outer husimi')

    q_list, p_list, husimiData = husimi.file.read_husimi_csv('husimi_outer_em.csv')

    husimi.plot.plot_husimi(
        q_list, p_list, husimiData,
        fname='husimi_outer_em.png',
        )
    
    husimi.plot.plot_husimi(
        q_list, p_list, np.power(husimiData, 0.25),
        fname='husimi_outer_em_root.png',
        )


    return None


if __name__ == '__main__':
    main()