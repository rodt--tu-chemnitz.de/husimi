'''
This evaluates the husimi function for an example to demonstrate the package use
'''

# import husimi module
import sys
sys.path.append('../..')
import husimi

import numpy as np
from scipy.constants import speed_of_light




def main() -> None:
    #########################################
    # start
    #########################################

    print('Performing demo')

    print('- parameters:')

    frequency_THz = 593.4531246515971+0.032836927465673675j
    frequency_Hz = np.real(frequency_THz) * 1e12
    wavelength_m = speed_of_light / frequency_Hz


    refractiveIndex_inner = 3.
    refractiveIndex_outer = 1.

    wavenumber_per_m_inner = 2 * np.pi * refractiveIndex_inner / wavelength_m
    wavenumber_per_m_outer = 2 * np.pi * refractiveIndex_outer / wavelength_m


    lengthScale = 1e-6 # micrometer is unit length

    print('\t-> freq :\t{0:.6e} Hz'.format(frequency_Hz))
    print('\t-> wavelen :\t{0:.6e} m'.format(wavelength_m))
    print('\t-> wavenumber (outside) :\t{0:.6e} m^-1'.format(wavenumber_per_m_outer))
    print('\t-> wavenumber (inside) :\t{0:.6e} m^-1'.format(wavenumber_per_m_inner))


    #########################################
    # read data
    #########################################

    print('- reading data')


    fileName_boundaryFunction = 'boundaryfunc.txt'

    
    data = husimi.file.read_comsol_file(fileName_boundaryFunction, skip_header=5)
    
    arcLength_list:"list[float]" = data[1]
    field_normal:"list[float]" = data[4]
    field_derivative:"list[float]" = data[5]

    cavityArcLength:"float" = np.max(arcLength_list)

    # norming fields
    field_normal_absmax = np.max(np.abs(field_normal))
    field_normal = field_normal / field_normal_absmax
    field_derivative = field_derivative / field_normal_absmax
    
    n_qPoints = 401
    q_list_unitless = np.linspace(0,1,n_qPoints)

    n_pPoints = 199
    p_list_unitless = np.linspace(-1,1,n_pPoints+2)[1:-1]



    #########################################
    # husimi calculation
    #########################################
    
    print('- calculating husimi')
    
    print('\t-> inner incident')
    husimi_inner_inc = husimi.calc.calculate_husimi(
        q_list_unitless,        p_list_unitless,
        field_normal,           field_derivative,
        arcLength_list,         cavityArcLength,
        wavenumber_per_m_inner, wavenumber_per_m_outer,
        refractiveIndex_inner,  refractiveIndex_outer,
        loc='inside',           direction='incident',
        lengthScale=lengthScale
        )
    

    print('\t-> outer emerging')
    husimi_outer_em = husimi.calc.calculate_husimi(
        q_list_unitless,        p_list_unitless,
        field_normal,           field_derivative,
        arcLength_list,         cavityArcLength,
        wavenumber_per_m_inner, wavenumber_per_m_outer,
        refractiveIndex_inner,  refractiveIndex_outer,
        loc='outside',          direction='emerging',
        lengthScale=lengthScale
        )
    
    
    #########################################
    # saving husimi functions
    #########################################
    
    print('- saving husimis')
    
    
    print('\t-> inner incident')
    
    husimi.file.save_husimi_to_csv(
        q_list_unitless, p_list_unitless,
        husimi_inner_inc,
        fname='husimi_inner_inc.csv'
        )
    
    
    print('\t-> outer emerging')
    
    husimi.file.save_husimi_to_csv(
        q_list_unitless, p_list_unitless,
        husimi_outer_em,
        fname='husimi_outer_em.csv'
        )


    return None


if __name__ == '__main__':
    main()